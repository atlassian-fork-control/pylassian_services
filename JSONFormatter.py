import logging
import json
import datetime


class JSONFormatter(logging.Formatter):
    """A formatter that renders log records as JSON objects.
    Format: {"timestamp":"...", "level":"...", "name":"...", "message":"..."}
    """
    def format(self, record):
        """Encode log record as JSON.
        """
        log = {
            "timestamp": self.formatTime(record, self.datefmt),
            "level": record.levelname,
            "name": record.name,
            "message": record.getMessage(),
        }
        if record.exc_info:
            log["traceback"] = self.formatException(record.exc_info)
        return json.dumps(log)

    def formatTime(self, record, date_format=None):
        """Override default to use strftime, e.g. to get microseconds.
        """
        created = datetime.datetime.fromtimestamp(record.created)
        if date_format:
            return created.strftime(date_format)
        else:
            return created.strftime("%Y-%m-%d %H:%M:%S") + ".%03d" % record.msecs