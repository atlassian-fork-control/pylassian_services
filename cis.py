import slumber
import pylassian


class CiService(object):
    """Wrapper that adds functionality on top of the basic CIS REST API.
    """
    def __init__(self, url):
        self.url = url
        session = pylassian.ResilientSession()
        session.verify = False        # set verify as False to avoid SSL checks
        self.service = slumber.API(
            self.url,
            session=session,
            append_slash=False,
        )

    def get_host_cis_details(self, key):
        return self.service.hostnames(key).get()