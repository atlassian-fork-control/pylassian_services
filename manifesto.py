"""Helper for interactions with Manifesto.
"""
import json
from time import sleep, time
from urlparse import urlparse
import slumber
import logging
from pylassian import utc_from_str, explode_artifacts
from pylassian import ResilientSession

logger = logging.getLogger(__name__)


def deployment_duration(status, current_time):
    """Get duration of deployment as timedelta.
    """
    if status.get('endTime'):
        return utc_from_str(status['endTime']) - utc_from_str(status['startTime'])
    else:
        return current_time - utc_from_str(status['startTime'])


class ManifestoService(object):
    """Wrapper that adds functionality on top of the basic Manifesto REST API.
    """
    start_interval = 5
    poll_interval = 60

    def __init__(self, auth, hypnotad_service, url):
        self.url = url
        self.user = auth[0]
        self.service = slumber.API(
            self.url,
            session=ResilientSession(),
            auth=auth,
        )
        # Hypnotoad does not like trailing slash.
        self.hypnotoad = slumber.API(
            hypnotad_service,
            session=ResilientSession(),
            auth=auth,
            append_slash=False,
        )
        self.failed = {}
        logger.info("Using Manifesto at %s as %s", self.url, self.user)

    def get_manifest(self, name):
        return self.service.api.env(name).get()

    def named_hash(self, name):
        return self.get_manifest(name).get('hash')

    def set_hash(self, name, new_hash):
        return self.service.api.hash(name)(new_hash).post()

    def get_environments(self):
        return dict([(e['environment'], e) for e in self.service.api.summary.get()['environments']])

    def get_diff(self, old, new):
        return self.service.api.hash.diff(old + '/' + new).get()

    def custom_hash(self, artifacts, environment):
        return self.service.api.manifest(environment).post(artifacts)

    def get_messages(self, request_id):
        return self.service.api.progress(request_id).get()

    def change_lock(self, name, locked):
        try:
            return self.service.api(name).lock.put({'lock': locked})
        except ValueError:
            pass  # ignore plain text in success response from Manifesto

    @staticmethod
    def construct_change_set(artefacts, version):
        """Construct change set to be posted as payload to manifesto
        """
        change = {"products": [], "plugins": []}
        for artefact in explode_artifacts(artefacts):
            if isinstance(artefact, basestring):
                change['products'].append(dict(
                    artifact=artefact,
                    version=version,
                ))
            else:
                for app in artefact[1]:
                    change['plugins'].append(dict(
                        artifact=artefact[0],
                        version=version,
                        product=app,
                    ))
        return {'set': change}

    def update_manifest(self, manifest, artefacts, version):
        """Update named manifest with given artefacts and return status of confirmed task.

        Will wait until the manifest has been updated, but may not wait until the software
        tree has been updated everywhere and the task is complete.
        """
        # Request promote task to change manifest without triggering upgrades.
        request = self.construct_change_set(artefacts=artefacts, version=version)
        logger.debug("Sending %s update request: %s", manifest, request)
        response = self.service.api.env(manifest).post(request, forcePluginDeployments=False)
        # Return unique task id.
        return response['id']

    def create_custom_manifest(self, manifest, artefacts, version):
        """Create custom manifest and return manifesto task ID.
        """
        request = self.construct_change_set(artefacts=artefacts, version=version)
        logger.debug("Sending custom hash request for %s with products: %s", manifest, request)
        response = self.service.api.manifest(manifest).post(request)
        # Return unique task id.
        return response['id']

    def get_manifest_details(self, manifest):
        """Get the icebat format contents of a given hash.
        """
        logger.debug("Fetching manifest with hash %s", manifest)
        response = self.service.static(manifest).get()
        return response

    def wait_for_confirmation(self, uuid):
        """Poll promotion task with given uuid until manifest updated.
        :param uuid: unique ID of existing promotion task
        :return: status details of promotion task when confirmed
        :rtype: dict
        """
        done = False
        wait = self.start_interval
        logger.info("Checking task %s status every %ds ...", uuid, self.poll_interval)
        status = 'UNKNOWN'
        while not done:
            sleep(wait)
            # Request status ignoring any transient errors.
            status = self.service.api.progress(uuid).get()
            logger.info(
                "... %s",
                status.get('status'),
            )
            # Done if status says so and also none queued or in progress
            # because status can be FAILED due to DE errors but task is still running.
            done = status['status'] not in ["QUEUED", "IN_PROGRESS"]
            wait = self.poll_interval
        return status

    def query_deployment(self, kind, selectors, options):
        """Get number of hosts that would be selected by a deployment.
        """
        info = dict(selectors=selectors, config=options)
        logger.debug("Sending %s deployment query\n%s", kind, json.dumps(info, indent=2))
        query = self.service.deployment.verify(kind).post(info)
        total = query['selectedHostCount']
        logger.debug("Query selected %d instances", total)
        return total

    def start_deployment(self, kind, selectors, options):
        """Start a deployment with given options and selectors.
        """
        info = dict(selectors=selectors, config=options)
        logger.debug("Sending %s deployment request\n%s", kind, json.dumps(info, indent=2))
        self.failed = {}
        create = self.service.deployment.deploy(kind).post(info)
        # Get deployment id and show dashboard link.
        uuid = create['id']
        dash = "%s/ui/dashboard?deployment_id=%s" % (self.url, uuid)
        logger.info("Deployment dashboard: [%s]", dash)
        return uuid, dash

    def wait_for_completion(self, uuid, alert=False):
        """Poll deployment task with given uuid until complete.

        :param uuid: unique ID of existing deployment task
        :param alert: post alerts to Incidash for failed instances if True
        :return: status details of deployment task when complete
        :rtype: dict
        """
        done = False
        wait = self.start_interval
        logger.info("Checking task %s status every %ds ...", uuid, self.poll_interval)
        while not done:
            sleep(wait)
            # Request status ignoring any transient errors.
            status = self.service.deployment.report(uuid).get()
            logger.info(
                "... %s with %d selected, %d queued, %d running, %d successful, %d failed, %d aborted out of %d",
                status.get('status'),
                status.get('selected'),
                status.get('queued'),
                status.get('inProgress'),
                status.get('complete'),
                status.get('failed'),
                status.get('aborted'),
                status.get('total'),
            )
            if alert and status.get('failed') > self.upgrade_alerts_posted():
                self.post_upgrade_failure_alerts(uuid)
            # Done if status says so and also none queued or in progress
            # because status can be FAILED due to DE errors but task is still running.
            done = (
                status['status'] not in ["DISTRIBUTING", "IN_PROGRESS"] and
                not status.get('queued') and
                not status.get('inProgress')
            )
            wait = self.poll_interval
        # Check for DE errors.
        for failure in ['undistributed', 'failed']:
            key = "%sDeploymentEngines" % failure
            if status.get(key):
                logger.warn("... Some %s DEs: %d", failure, len(status[key]))
                for item in status[key]:
                    logger.debug("    %s = %s", item['deploymentEngine'], item['error'])
        # Warn that status is FAILED on DE errors.
        if status.get('failedDeploymentEngines') and status['status'] == "FAILED":
            logger.warning(
                "Status is %s due to DE failures - check %s dashboard for upgrade failures",
                status['status'],
                status['deploymentId'],
            )
        return status

    def upgrade_alerts_posted(self):
        return len(self.failed)

    def post_upgrade_failure_alerts(self, uuid):
        """Create Incidash alerts for any new failed hosts.

        Finds any new failed host reports and posts details in an alert to hypnotoad
        so that it appears on Incidash.

        :param uuid: deployment task id
        :param failed: known error reports indexed by hostname
        """
        # Get report of failures per host.
        errors = self.service.deployment.report(uuid).hosts.get(status='failed')
        for error in errors['hosts']:
            host = error.get('hostname')
            if host and host not in self.failed:
                self.failed[host] = error
                fail = error['failureMessage'].strip()
                zone = urlparse(error['origin']).hostname.split('.')[1]
                # Send DE alert to appear on Incidash.
                try:
                    self.hypnotoad.alerts.post(dict(
                        hostname=host,
                        alert_string=fail,
                        platform="ondemand",
                        service="deployment-engine",
                        location=zone,
                        status=2,  # critical
                        timestamp=int(time() * 1000),
                    ))
                    logger.warn("Alert posted for %s in %s: %s", host, zone, fail)
                except Exception as e:
                    logger.warn(
                        "Unable to post alert for %s in %s (%s): %s",
                        host, zone, fail, e,
                    )
