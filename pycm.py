import slumber
from slumber.exceptions import HttpNotFoundError
import pylassian
from multiprocessing.dummy import Pool
import logging
from itertools import chain


logger = logging.getLogger(__name__)


class PycmService(object):
    """Wrapper that adds functionality on top of the basic pycm REST API.
    """
    def __init__(self, auth):
        self.auth = auth
        self.pycm_expands = [
            'detail',
            'version',
            'destroyed',
            'maintenance',
            ]

    def query_zone(self, url, zone, key):
        url = url % zone
        session = pylassian.ResilientSession()
        session.verify = False
        service = slumber.API(
            url + "/pycm/v1.0",
            session=session,
            auth=self.auth,
            append_slash=False,
        )
        try:
            result = service.host(key).get(expands=self.pycm_expands)
            return result
        except HttpNotFoundError:
            pass

    def get_host_details(self, key, zones, base_url):
        multi = Pool(len(zones))
        found = list(multi.map(lambda z: self.query_zone(url=base_url,
                                                         key=key,
                                                         zone=z), zones))
        instance_data = [x for x in found if x is not None]    # Trim None values from the list
        return instance_data




